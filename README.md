# 6. CKA Pratice Exam Part 6 ACG

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs

1. Drainer le Worker node 1

2. Créer un pod qui sera planifié uniquement sur les nœuds avec un label spécifique

# Contexte

Cet atelier propose des scénarios pratiques pour vous aider à vous préparer à l'examen Certified Kubernetes Administrator (CKA). Des tâches à accomplir vous seront présentées, ainsi que des serveurs et/ou un cluster Kubernetes existant pour les accomplir. Vous devrez utiliser vos connaissances de Kubernetes pour mener à bien les tâches fournies, un peu comme vous le feriez sur le véritable examen CKA. Bonne chance!

Cette question utilise le acgk8scluster. Après vous être connecté au serveur d'examen, passez au contexte correct avec la commande kubectl config use-context acgk8s.

Chacun des objectifs représente une tâche que vous devrez accomplir en utilisant le cluster et les serveurs disponibles. Lisez attentivement chaque objectif et accomplissez la tâche spécifiée.

Pour certains objectifs, vous devrez peut-être vous connecter à d'autres nœuds ou serveurs à partir du serveur d'examen. Vous pouvez le faire en utilisant le nom d'hôte/nom de nœud, c'est-à-dire ssh acgk8s-worker1.

Remarque : Vous ne pouvez pas vous connecter à un autre nœud, ni l'utiliser kubectlpour vous connecter au cluster, à partir d'un nœud autre que le nœud racine. Une fois que vous avez terminé les tâches nécessaires sur un serveur, assurez-vous de exitrevenir au nœud racine avant de continuer.

Si vous devez assumer les privilèges root sur un serveur, vous pouvez le faire avec sudo -i.

Vous pouvez exécuter le script de vérification situé à /home/cloud_user/verify.shtout moment pour vérifier votre travail !

>![Alt text](img/image.png)

# Application

## Étape 1 : Connexion au Serveur

1. Connectez-vous au serveur à l'aide des informations d'identification fournies :

```sh
ssh cloud_user@<PUBLIC_IP_ADDRESS>
```

## Étape 2 : Vider le Nœud `worker1`

1. Passez au contexte `acgk8s` :

```sh
kubectl config use-context acgk8s
```

2. Essayez de drainer le nœud `worker1` :

```sh
kubectl drain acgk8s-worker1
```

3. Si le nœud ne se draine pas correctement, remplacez les erreurs et videz le nœud :

```sh
kubectl drain acgk8s-worker1 --delete-local-data --ignore-daemonsets --force
```

>![Alt text](img/image-3.png)

4. Vérifiez l'état des objectifs de l'examen :

```sh
./verify.sh
```

## Étape 3 : Créer un Pod qui Sera Planifié Uniquement sur les Nœuds avec une Étiquette Spécifique

1. Ajoutez l'étiquette `disk=fast` au nœud `worker2` :

```sh
kubectl label nodes acgk8s-worker2 disk=fast
```

il est possible de vérifier que le nouveau label à bien été affecté au node en entrant la commande : 

```sh
kubectl describe node acgk8s-worker2
```

>![Alt text](img/image-1.png)


2. Créez un fichier YAML nommé `fast-nginx.yml` :

```sh
vim fast-nginx.yml
```

3. Dans le fichier, collez ce qui suit :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: fast-nginx
  namespace: dev
spec:
  nodeSelector:
    disk: fast
  containers:
  - name: nginx
    image: nginx
```

Ref doc : https://kubernetes.io/docs/tasks/configure-pod-container/assign-pods-nodes/#create-a-pod-that-gets-scheduled-to-your-chosen-node

4. Enregistrez le fichier et quittez l'éditeur :

   - Appuyez sur `ESC`
   - Tapez `:wq` et appuyez sur `Entrée`

5. Créez le pod `fast-nginx` :

```sh
kubectl create -f fast-nginx.yml
```


6. Vérifiez l'état du pod :

```sh
kubectl get pod fast-nginx -n dev -o wide
```

>![Alt text](img/image-2.png)

7. Vérifiez l'état des objectifs de l'examen :

```sh
./verify.sh
```

En suivant ces étapes, vous aurez vidé le nœud `worker1`, ajouté une étiquette spécifique au nœud `worker2`, et créé un pod planifié uniquement sur les nœuds avec cette étiquette spécifique.